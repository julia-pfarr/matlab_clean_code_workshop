# Automating

## MATLAB CI/CD in gitlab

- documentation: https://github.com/mathworks/matlab-gitlab-ci-template
- template:
  https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/MATLAB.gitlab-ci.yml
